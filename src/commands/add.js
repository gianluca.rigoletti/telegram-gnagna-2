const bot = require('../modules').bot
const trimText = require('../utils').trimText
const readStore = require('../utils').readStore
const messages = require('../messages')
const configs = require('../configs')
const writeStore = require('../utils').writeStore
const request = require('../modules').request
const db = require('../modules').db
const getNavigationTree = require('../utils').getNavigationTree
const getBackAndNextNav = require('../utils').getBackAndNextNav
// let waitForUsername = false

bot.on(/➕ Aggiungi ➕/, (msg) => {
    bot.sendMessage(msg.chat.id, messages.addNew, {
        replyMarkup: {
            force_reply: true
        }
    })
})

bot.on('text', (msg) => {
    let command = null
    if (typeof msg.reply_to_message !== 'undefined') {
        command = msg.reply_to_message.text
    }
    if (command && command === messages.addNew) {
        let username = trimText(msg.text)
        // check if chat id is already present in the store. Create one if it 
        // doesn't exist.
        if (!db.has(msg.chat.id).value()) {
            db.set(msg.chat.id, {
                'usernames': []
            })
            .write()
        }
        // strip the @ character
        username = msg.text
            .trim()
            .replace('@', '');
        const usernameIsValid = /^[a-zA-Z0-9._]+$/.test(username)
        if (!usernameIsValid) {
            console.log(username)
            return msg.reply.text(messages.usernameIsNotValid)
        }
        // Find the username and add it to the relative chat id
        addUsername(username, msg.chat.id)
    }
})

const addUsername = (username, id) => {
    let success = false;
    let errorMessage = false;
    let responseData = '';
    // Check if username exists and add it to the store
    console.log(configs.baseUrl + username + configs.apiRoute)
    request
        .get(configs.baseUrl + username + configs.apiRoute)
        .on('error', (err) => {
            console.log(err);
            errorMessage = messages.generalError;
        })
        .on('response', (response) => {
            // add the user to the usernames store
            // const usernames = store[msg.chat.id].usernames;
            if (response.statusCode === 200) {
                success = true;
            } else {
                errorMessage = messages.usernameNotFound;
            }

        })
        .on('data', (chunk) => {
            const chunkString = (chunk) ? chunk.toString('utf8') : '';
            responseData += chunk.toString('utf8')
        })
        .on('end', () => {
            if (success) {
                const jsonData = JSON.parse(responseData)
                const fullName = jsonData.user.full_name
                // check if username is already present in the list
                const hasUsernames = db.get(id)
                            .get('usernames')
                            .find({ username: username })
                            .get('username')
                            .value()
                if (hasUsernames === undefined) {
                    db.get(id)
                    .get('usernames')
                    .push({
                        'username': username,
                        'link': configs.baseUrl + username,
                        'full_name': fullName
                    })
                    .write()
                } else {
                    errorMessage = messages.usernameAlreadyAdded
                }
            }
            // If everything is right send a confirmation to the user
            const replyMessage = errorMessage ? errorMessage : messages.successfulAdd;
            console.log(replyMessage)

            db.get(id)
                .set('navigation_state', '➕ Aggiungi ➕')
                .write()

            // Set the layout for the navigation
            const navigationTree = getNavigationTree(id)
            const { back, next } = getBackAndNextNav(navigationTree)
            const layout = next.filter(el => el !== '/start')
                                .map(el => [el])
            const keyboardLayout = bot.keyboard(layout, {
                resize: true,
                once: true
            })
             
            return bot.sendMessage(id, replyMessage, { 
                parseMode: 'Markdown',
                replyMarkup: keyboardLayout
            });
        })

}