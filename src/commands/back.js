const db = require('../modules').db
const bot = require('../modules').bot
const getNavigationTree = require('../utils').getNavigationTree
const getBackAndNextNav = require('../utils').getBackAndNextNav
const getUsernamesList = require('../utils').getUsernamesList
const getFullnameList = require('../utils').getFullnameList
const getLinksList = require('../utils').getLinksList

bot.on(/🔙/, (msg) => {
    const state = db.get(msg.chat.id)
                    .get('navigation_state')
                    .value()
    const navigationTree = getNavigationTree(msg.chat.id)
    let { back, next } = getBackAndNextNav(navigationTree, state)
    if (back === undefined || !back) {
        back  = Object.keys(navigationTree)
    }
    const layout = back.filter(el => el !== '/start')
                        .map(el => [el])
    const keyboard = bot.keyboard(layout, {
        once: true,
        selective: true,
        resize: true
    })
    return bot.sendMessage(msg.chat.id, '🔙', {
        replyMarkup: keyboard
    })
})