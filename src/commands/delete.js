const bot = require('../modules').bot
const trimText = require('../utils').trimText
const readStore = require('../utils').readStore
const messages = require('../messages')
const configs = require('../configs')
const writeStore = require('../utils').writeStore
const getUsernamesList = require('../utils').getUsernamesList
const db = require('../modules').db
const outdent = require('outdent')


bot.on(/⛔ Rimuovi ⛔/, (msg) => {
    const items = db.get(msg.chat.id)
                    .get('usernames')
                    .value()
    if (items.length === 0) {
        return bot.sendMessage(msg.chat.id, replyMessage)
    }

    db.get(msg.chat.id)
        .set('navigation_state', '⛔️ Rimuovi ⛔️')
        .write()

    return bot.sendMessage(msg.chat.id, messages.askRemoveUsername, {
        replyMarkup: getKeyboardLayoutForList(items)
    })
})

bot.on('text', (msg) => {
    if (msg.text.includes('❌') && msg.text.includes('|')) {
        let username = trimText(msg.text.split('|')[1])
        // check if chat id is already present in the store. Create one if it 
        // doesn't exist.
        const items = db.get(msg.chat.id)
                        .get('usernames')
                        .value()
        const hasUsername = db.get(msg.chat.id)
                                .get('usernames')
                                .remove({username: username})
                                .write()

        return bot.sendMessage(msg.chat.id, messages.removedUsername, {
            replyMarkup: getKeyboardLayoutForList(items)
        })
    }

})

const getKeyboardLayoutForList = (items) => {
    const layout = items.map((el) => {
        return [outdent`
            ❌ ${el.full_name} | ${el.username}
        `]
    })
    layout.unshift(['🔙'])
    console.log(layout)
    return bot.keyboard(layout, {
        once: true,
        selective: true,
        resize: true
    })

}