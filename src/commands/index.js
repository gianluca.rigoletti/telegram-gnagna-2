const modules = require('../modules')
const messages = require('../messages')
const store = require('../store');
const configs = require('../configs')

module.exports = {
    modules: modules,
    messages: messages,
    store: store,
    configs: configs
}
