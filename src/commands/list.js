const bot = require('../modules').bot
const readStore = require('../utils').readStore
const messages = require('../messages')
const getNavigationTree = require('../utils').getNavigationTree
const getBackAndNextNav = require('../utils').getBackAndNextNav
const getUsernamesList = require('../utils').getUsernamesList
const getFullnameList = require('../utils').getFullnameList
const getLinksList = require('../utils').getLinksList
const outdent = require('outdent')
const db = require('../modules').db


bot.on(/📃 Lista 📃/, (msg) => {
    let replyMessage = messages.emptyUsernames
    const items = db.get(msg.chat.id)
                    .get('usernames')
                    .value()

    if (items === undefined || items.length === 0) {
        return bot.sendMessage(msg.chat.id, replyMessage)
    }

    replyMessage = items.map((el) => {
        return outdent`
            👸 __${el.full_name}__
            📷 \`${el.username}\`
            🌏 ${el.link}
            〰️ 〰️ 〰️ 〰️ 〰️ 〰️ 
        `
    }).join('\n')

    db.get(msg.chat.id)
        .set('navigation_state', '📃 Lista 📃')
        .write()

    return bot.sendMessage(msg.chat.id, replyMessage, {
        webPreview: false,
        parseMode: 'Markdown'
    })

})