const bot = require('../modules').bot
const getNavigationTree = require('../utils').getNavigationTree
const getBackAndNextNav = require('../utils').getBackAndNextNav
const messages = require('../messages')
const db = require('../modules').db

bot.on('/start', (msg) => {
    const navigationTree = getNavigationTree(msg.chat.id)
    const { back, next } = getBackAndNextNav(navigationTree)
    const layout = next.filter(el => el !== '/start')
                        .map(el => [el])
    const keyboardLayout = bot.keyboard(layout, {
        resize: true,
        once: true,
        remove: true
    })

    db.get(msg.chat.id)
        .set('navigation_state', '/start')
        .write()

    return bot.sendMessage(msg.chat.id, messages.startChat, { 
        parseMode: 'Markdown',
        replyMarkup: keyboardLayout
    })
})