const baseUrl = 'https://www.instagram.com/';
const mediaRoute = '/media';
const apiRoute = '?__a=1'
const pollTime = 60000

module.exports = {
    baseUrl: baseUrl,
    mediaRoute: mediaRoute,
    apiRoute: apiRoute,
    pollTime: pollTime
}