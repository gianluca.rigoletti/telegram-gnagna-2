const readStore = require('./utils').readStore
const writeStore = require('./utils').writeStore
const pollFromInstagram = require('./utils').pollFromInstagram
const pollTime = require('./configs').pollTime


const pollUsers = () => {
    // let fileData = JSON.parse(fs.readFileSync('./store.json', 'utf8'));
    let store = readStore('./src/store.json')
    console.log(JSON.stringify(store))
    const chatIds = Object.keys(store)
    let errorLog;
    chatIds.map((chatId) => {
        store[chatId].usernames.map((user) => {
            pollFromInstagram(user, chatId, store);
        })
    })
}

setInterval(pollUsers, pollTime)