const startChat = `Questo bot serve a fare buon uso di Instagram come fonte di *gnanga.*`
const addNew = `Inserisci lo username instagram`
const successfulAdd = 'Bravo bagai, la gnagna è stata aggiunta'
const notFoundUsername = 'Gnagna non trovata su instagram'
const usernameAlreadyAdded = 'Gnagna già aggiunta, vecchio marpione'
const generalError = 'Qualcosa è andato storto. Riprova'
const removedUsername = 'Hai rimosso lo username'
const askRemoveUsername = 'Seleziona lo username da rimuovere'
const emptyText = 'Non hai scritto niente'
const emptyUsernames = 'Non hai gnagna salvata'
const usernameNotPresent = 'Lo username non è nella tua lista'


module.exports = {
   startChat: startChat,
   addNew: addNew,
   successfulAdd: successfulAdd,
   notFoundUsername: notFoundUsername,
   usernameAlreadyAdded: usernameAlreadyAdded,
   generalError: generalError,
   askRemoveUsername: askRemoveUsername,
   removedUsername: removedUsername,
   emptyText: emptyText,
   emptyUsernames: emptyUsernames,
   usernameNotPresent : usernameNotPresent 
}