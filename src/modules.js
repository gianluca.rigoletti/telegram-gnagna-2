const telebot = require('telebot');
const fs = require('fs');
const store = require('./store.json');
const util = require('util')
const config = require('dotenv').config()
const bot = new telebot(process.env.TOKEN);
const request = require('request');
const rp = require('request-promise');
const baseUrl = 'https://www.instagram.com/';
const mediaRoute = '/media';
const apiRoute = '?__a=1'
const StringDecoder = require('string_decoder').StringDecoder;
const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const adapter = new FileSync('./src/store.json')
const db = low(adapter)
const axios = require('axios')

module.exports = {
   telebot: telebot,
   fs: fs,
   store: store,
   util: util,
   config: config,
   bot: bot,
   request: request,
   rp: rp,
   baseUrl: baseUrl,
   mediaRoute: mediaRoute,
   apiRoute: apiRoute,
   StringDecoder : StringDecoder,
   db: db,
   axios: axios
}