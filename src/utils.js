const modules = require('./modules')
const { StringDecoder } = require('string_decoder');
const configs = require('./configs')
const db = require('./modules').db
const axios = require('./modules').axios
const bot = require('./modules').bot


const getUsernamesList = (chatId) => {
    return db.get(chatId)
    .get('usernames')
    .map('username')
    .value()
}


const getFullnameList = (chatId) => {
    return db.get(chatId)
    .get('usernames')
    .map('full_name')
    .value()
}

const getLinksList = (chatId) => {
    return db.get(chatId)
    .get('usernames')
    .map('link')
    .value()
}


const readStore = (path) => {
    let store = null;
    try {
        store = JSON.parse(modules.fs.readFileSync(path, 'utf8'));
    } catch(e) {
        console.log(e)
    }
    return store;
}


const writeStore = (store, callback) => {
    const storePath = './src/store.json'
    let error = null
    modules.fs.writeFile(storePath, JSON.stringify(store, null, 2), 'utf8', (err) => {
        if (err) {
            error = err
        }
    })

    callback(error)
}


const trimText = (text, command) => {
    const botName = '@polemicatest_bot'
    const trimmedText = text
        .trim()
        .replace(`${command}${botName}`, '')
        .replace(`${command}`, '')

    return trimmedText
}


/*const getUsernamesList = (chatId, store) => {
    console.log(store)
    if (typeof store[chatId].usernames === 'undefined') {
        return false
    }
    return store[chatId].usernames.map((el) => el.full_name)
}*/


const pollFromInstagram = (user, chatId, store) => {
    // perform a modules.request
    // 
    // 
    axios.get(configs.baseUrl + user.username + configs.mediaRoute)
        .then((response) => {
            const lastItem = response.data.items[0]
            if (lastItem !== undefined) {
                const link = lastItem.link
                const date = lastItem.created_time
                const lastCheck = db.get(chatId)
                                    .get('usernames')
                                    .find({ username: user.username })
                                    .value()
                                    .last_check
                const isNewPost = lastCheck < date.toString()
                if (lastCheck === undefined || !lastCheck || isNewPost) {
                    db.get(chatId)
                        .get('usernames')
                        .find({username: user})
                        .set('last_check', date)
                        .write()
                }
                if (isNewPost) {
                    bot.sendMessage(chatId, link)
                }
            }
        })
        .catch((error) => {
            console.error('Si è verificato un errore sul poll da instagram:')
            console.error(error)
        })
}


const getNavigationTree = (chatId) => {
    let usernameList = getUsernamesList(chatId)
    let objectList = {}
    usernameList.map(el => {
        objectList[el] = {}
    })
    return {
        '/start': {},
        '➕ Aggiungi ➕': {},
        '📃 Lista 📃': {
            objectList
        },
        '⛔ Rimuovi ⛔': {
            objectList
        }
    }
}


const getBackAndNextNav = (obj, pattern) => {
    if (pattern === undefined || !pattern) {
        return {
            back: Object.keys(obj),
            next: Object.keys(obj)
        }
    }
    let nextKeys = null
    let backKeys = null
    let prevKeys = null
    const navigate = (obj, pattern, tempKeys) => {
        const keys = Object.keys(obj)
        const length = keys.length
        for (let i = 0; i < length; i++) {
            if (pattern === keys[i]) {
                prevKeys = tempKeys
                nextKeys = Object.keys(obj[keys[i]])
                return {
                    back: prevKeys,
                    next: nextKeys
                }
            }
            if (obj[keys[i]] !== undefined) {
                backKeys = keys
                navigate(obj[keys[i]], pattern, keys)
            }
        }
        return {
            back: prevKeys,
            next: nextKeys
        }
    }
    return navigate(obj, pattern)    
}

module.exports = {
    readStore: readStore,
    trimText: trimText,
    writeStore: writeStore,
    getNavigationTree: getNavigationTree,
    getUsernamesList: getUsernamesList,
    pollFromInstagram: pollFromInstagram,
    getFullnameList: getFullnameList,
    getBackAndNextNav: getBackAndNextNav,
    getLinksList: getLinksList
}